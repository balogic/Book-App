Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'sessions',
    registrations: 'registrations'
  }
  root 'dashboard#home'
  get '/books' => 'books#index'
  get '/users' => 'users#index'
  patch '/users/:id' => 'users#update', as: 'update_user'
  get '/books/new' => 'books#new'
  post 'books' => 'books#create'
  get '/category/new' => 'category#new'
  get '/categories' => 'category#index'
  post 'categories' => 'category#create'
  get 'categories/:id' => 'category#show', as: 'category'
  get 'users/:id' => 'users#show', as: 'user_show'
  get '/users/:id/edit' => 'users#edit', as: 'edit_user'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
