class RegistrationsController < Devise::SessionsController
  skip_before_action :authenticate_user!
  protect_from_forgery prepend: true
  layout 'session'
  def create
    log_response registration_params
    @user = User.new(registration_params)
    @user.save
    redirect_to root_path
    log_response @user.errors.full_messages
  end

  private

  def registration_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :username)
  end
end
