class UsersController < ApplicationController
  layout 'dashboard'
  skip_before_action :authenticate_user!
  before_action :set_user, only: [:edit, :show, :update]

  def index
    @users = User.all
  end

  def edit
  end

  def update
    @user.update(user_params)
    redirect_to root_path
  end

  def show
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :email, :location, :website, :twitter_username, :bio, :image, :tagline)
  end
end
