class DashboardController < ApplicationController
  layout 'dashboard'
  def home
    log_response current_user
    @books = Book.all
    @users = User.all
    @categories = Category.all
  end
end
