class SessionsController < Devise::SessionsController
  protect_from_forgery prepend: true
  layout 'session'
end
