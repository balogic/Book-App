class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  def log_response(object)
    Rails.logger.debug { '#' * 50 }
    Rails.logger.debug { object.inspect }
    Rails.logger.debug { '#' * 50 }
  end
end
