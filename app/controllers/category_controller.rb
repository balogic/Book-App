class CategoryController < ApplicationController
  layout 'dashboard'
  def new
    @category = Category.new
  end

  def show
    @category = Category.find(params[:id])
    @books = @category.books
  end

  def index
    @categories = Category.all
  end

  def create
    @category = Category.new category_params
    @category.save
    redirect_to root_path
  end

  private

  def category_params
    params.require(:category).permit(:name, :description)
  end
end
