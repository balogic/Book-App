class BooksController < ApplicationController
  layout 'dashboard'
  def new
    @book = Book.new
  end

  def create
    @book = Book.new(book_params)
    @book.save
    log_response @book.errors.full_messages
    redirect_to books_path
  end

  def index
    @books = Book.all
  end

  private

  def book_params
    params.require(:book).permit(:title, :author, :image, :user_id, :category_id)
  end
end
