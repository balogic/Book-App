class AddTitleToBooks < ActiveRecord::Migration[5.1]
  def up
    add_column :books, :title, :string
    remove_column :books, :name, :string
  end

  def down
    add_column :books, :name, :string
    remove_column :books, :title, :string
  end
end
