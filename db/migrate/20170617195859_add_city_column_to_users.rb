class AddCityColumnToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :location, :string
    add_column :users, :image, :string
    add_column :users, :tagline, :string
    add_column :users, :website, :string
    add_column :users, :twitter_username, :string
    add_column :users, :bio, :text
  end
end
