class ChangeTypeInCategory < ActiveRecord::Migration[5.1]
  def change
    remove_column :categories, :type
    add_column :categories, :name, :string
  end
end
