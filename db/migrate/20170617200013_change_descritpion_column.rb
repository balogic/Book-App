class ChangeDescritpionColumn < ActiveRecord::Migration[5.1]
  def up
    remove_column :books, :description
    remove_column :categories, :description
    add_column :books, :description, :text
    add_column :categories, :description, :text
  end

  def down
    remove_column :books, :description
    remove_column :categories, :description
    add_column :books, :description, :string
    add_column :categories, :description, :string
  end
end
